
[![pipeline status](https://s2000-gitlab.mpibpc.mpg.de/thomas.hensel/multiflux_release/badges/main/pipeline.svg)](https://s2000-gitlab.mpibpc.mpg.de/thomas.hensel/multiflux_release/-/commits/main)


# Welcome

Welcome to the project documentation regarding our publication *Diffraction minima resolve point scatterers at tiny fractions (1/80) of the wavelength*.
Find here the details of our analysis routines, some example code and everything needed to reproduce our findings.


Pre-print: [![DOI](https://img.shields.io/badge/DOI-10.1101/2024.01.24.576982-b31b1b?logo=arxiv&logoColor=red)](https://doi.org/10.1101/2024.01.24.576982)

Data: [![Zenodo](https://zenodo.org/badge/DOI/10.5281/zenodo.10625021.svg)](https://doi.org/10.5281/zenodo.10625021)

Orcid: [![Orcid](https://img.shields.io/badge/orcid-A6CE39?style=for-the-badge&logo=orcid&logoColor=white)](https://orcid.org/0000-0002-7497-3329)

X (Twitter): [![X (Twitter)](https://img.shields.io/badge/X-000000?style=for-the-badge&logo=x&logoColor=white)](https://twitter.com/HenselWork)

![SegmentLocal](src/figures/DynamicDataFigure/data/IntroAni.gif "segment")

## Repository Description
This repository contains the code and documentation needed to reproduce the results of our publication *Diffraction minima resolve point scatterers at tiny fractions (1/80) of the wavelength*.

## Data Availability
All data needed to reproduce our results can be retrieved via Zenodo under the link shown above. 

## Usage
Please consult the documentation for more information.