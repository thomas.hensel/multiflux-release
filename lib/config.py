"""
Set-up variables in this module.

Author: Thomas Arne Hensel
Date: 02/2024
"""

ROOT_DIR = '/home/thomas_hensel/partiallycoherentscattering'
DATA_DIR = '/mnt/d/datasets'
OUTPUT_DIR = '/mnt/d/artefacts'