==========
Welcome!
==========

These pages document the work behind our publication |pub_badge|.
All data needed to reproduce our findings can be found on Zenodo |data_badge|.

What is this all about? We took a video of two very close molecules - separated by about 1/10.000 of the thickness of a human hair.
That wasn't possible before. Have a look at our publication and this documentation to find out how we did it!

|ani|

To stay tuned, follow me on |X_badge| or have a look at my |orcid_badge|.

Check out the :doc:`./chapters/intro/sections/usage` section for further information, including how to
:ref:`install <installation>` the project.

In the following we have provided some examples in order to easily reproduce our research.
If you want to take a deep dive and reuse or modify routines, familiarize yourself with our code via the API documentation: :doc:`./apidocs/index`.



.. note::

   This project is under active development.

Contents
--------

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Table of Contents

   chapters/intro/intro
   
   chapters/examples/examples
   
   chapters/outro/outro
   apidocs/index