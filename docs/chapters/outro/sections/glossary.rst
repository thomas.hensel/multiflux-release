Glossary
===========

.. glossary::

    SMLM
      Single-molecule localization microscopy

    FOV
      Field of view
    
    MINFLUX
      Nanometer resolution imaging and tracking of fluorescent molecules with minimal photon fluxes

    NALM
      Nanometer-Localized Multiple Single-Molecule Fluorescence Microscopy

    PSF
      Point Spread Function

    STORM
      Stochastic Optical Reconstruction Microscopy

    FWHM
      Full Width Half Maximum
