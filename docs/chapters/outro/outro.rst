************
Outro
************

Thanks for stopping by. Let me know if you have any questions or remarks regarding our code and data.
Find me on |X_badge| or |orcid_badge|.

.. include:: /chapters/outro/sections/glossary.rst

.. include:: /chapters/outro/sections/bib.rst