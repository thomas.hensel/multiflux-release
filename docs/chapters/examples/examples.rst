===============
Using The Code
===============

In the following, I provide some guidance on how the code and the data available for this project can be used to reproduce our findings.

.. toctree::
   :titlesonly:
   :maxdepth: 1

   sections/intro
   sections/process_data
   sections/reproduce_figures
   sections/simulate_data