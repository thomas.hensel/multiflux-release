Usage
======

.. _installation:

Installation
---------------

I will give some hints on how to use this project here...

First, create a virtual environment for Python 3.9.

.. code-block:: console

   (base) $ python3 -m venv venv
 
Activate the environment with

.. code-block:: console

   (base) $ source venv/bin/activate

(.venv is the name of the virtual environment that has been initialized before.)
Now, clone the repository from GitLab:

.. code-block:: console

   (venv) $ git clone 'https://your-repo.git'

Load all dependencies from requirements.txt:

.. code-block:: console

   (venv) $ pip install -r requirements.txt

You're set and ready to go.
If you add new dependencies remeber to update the requirements file via

.. code-block:: console

   (venv) $ pip freeze -> requirements.txt


Comitting and Testing
---------------------
We want to avoid commits that corrupt the repository.
Pre-commit hooks allow to test for well-formatted code and faults.
Make sure pre-commit hooks are working. Install via

.. code-block:: console

   (venv) $ pre-commit install

Check if its working:

.. code-block:: console

   (venv) $ pre-commit run --all-files

Now, if you commit, these hooks should automatically run on the code and point out any errors.
Additionally, you should check if the code passess all specified tests.
This will be done automatically on each merge request.
By making sure your code passes the unit tests you can avoid unnecessary work.


Local Build
---------------------

Of course you can also build the documentation locally via

.. code-block:: console

   (venv) $ sphinx-build -b html ./docs/ ./docs/build

or parallelize the build with:

.. code-block:: console

   (venv) $ sphinx-build -j 4 -b html ./docs/ ./docs/build

use a Live-Server tool to view index.html in the build directory.


Configuration
=============
.. _configuration:

Make sure to configure the `lib.config` module to access the root directory of the repository and the needed datasets by specifying the paths there.