#############
Introduction
#############

In order to use this repository, you have to download it to your local machine and configure it according to the next sections.

.. toctree::
   :titlesonly:
   :maxdepth: 1

   sections/usage