# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

# import sphinx_bernard_theme
# import sphinx_theme

html_theme = "sphinx_rtd_theme"
bibtex_bibfiles = ["refs.bib"]
bibtex_default_style = "plain"
numfig = True


# -- Project information -----------------------------------------------------

project = "Resolve Point Scatterers with Diffraction Minima"
copyright = "2024, Thomas Arne Hensel"
author = "Thomas Arne Hensel"

# The full version, including alpha/beta/rc tags
release = "0.1"
todo_include_todos = True


# -- General configuration ---------------------------------------------------
rst_prolog = """
.. |pub_badge| image:: https://img.shields.io/badge/DOI-10.1101/2024.01.24.576982-b31b1b?logo=arxiv&logoColor=red
   :target: https://doi.org/10.1101/2024.01.24.576982
   :alt: DOI
   :height: 2ex

.. |data_badge| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.10625021.svg
   :target: https://doi.org/10.5281/zenodo.10625021
   :alt: Zenodo
   :height: 2ex

.. |orcid_badge| image:: https://img.shields.io/badge/orcid-A6CE39?style=for-the-badge&logo=orcid&logoColor=white
   :target: https://orcid.org/0000-0002-7497-3329
   :alt: ORCID
   :height: 2ex

.. |X_badge| image:: https://img.shields.io/badge/X-000000?style=for-the-badge&logo=x&logoColor=white
   :target: https://twitter.com/HenselWork
   :alt: X
   :height: 2ex

.. |ani| image:: ../src/figures/DynamicDataFigure/data/IntroAni.gif

"""

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_parser",
    "sphinx.ext.duration",
    "sphinx.ext.mathjax",
    "sphinx.ext.todo",
    "sphinxcontrib.bibtex",
    "IPython.sphinxext.ipython_directive",
    "IPython.sphinxext.ipython_console_highlighting",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.todo",
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
    'autodoc2',
    'nbsphinx'
]
autodoc2_packages = [
    "../lib",
]

# include markdown source files
source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "restructuredtext",
    ".md": "markdown",
}

mathjax3_config = {
    'tex': {'tags': 'ams', 'useLabelIds': True},
}

# Make sure the target is unique
autosectionlabel_prefix_document = True
todo_include_todos = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = "furo"
# html_theme = 'sphinx_bernard_theme'
# html_theme_path = [sphinx_bernard_theme.get_html_theme_path()]
# html_theme = 'stanford_theme'
# html_theme_path = [sphinx_theme.get_html_theme_path('stanford-theme')]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []
